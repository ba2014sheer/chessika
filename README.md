# Chessika

A very basic chess engine written to learn... c++ and stuff.

## Features

### Core
  - [x] BitBoard board representation 
  - [x] Move generation (KinderGarten implementation)
  - [x] PV Triangular table
  - [x] Basic UCI commands

### Evaluation
  - [x] Piece Square Tables evaluation

### Move ordering
  - [x] Previous PV first
  - [x] SEE on capture
  - [x] Killer Heuristic


### Search
  - [x] NegaMax evaluation
  - [x] Alpha-Beta Pruning
  - [x] Iterative deepening
  - [ ] Quiescence search
  - [x] Mate in N


# Thanks

All the folks of \#\#chessprogramming @ irc.freenode.net
