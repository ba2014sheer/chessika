# Ne joue pas la variation principale
bin/chessika -b "r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R b KQkq - 0 3" -d 3 -P "e7e5 f1b5 g8f6 b5c6 d7c6 b1c3" -h "Pe2e4 nb8c6 Ng1f3 pe7e5 Bf1b5"

# Miss 1 move : h1h2

mrpingouin@iris:~$ ./tmp/chessikadev/obj/chessika -t -b "7k/8/8/8/pP6/8/8/7K w - - 0 1" -d 1 -T
h1g1: 1
h1g2: 1
b4b5: 1
7k/8/8/8/pP6/8/8/7K w - - 0 1 ;D1 3

