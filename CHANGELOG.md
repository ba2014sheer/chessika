# Changelog

Here's the Elo evolution from the "blank engine" state

- 007 : Fixed time management : Elo difference: 335.56 +/- 31.16 (1035 rounds)

- 006 : Null-Move pruning : Elo difference: 48.43 +/- 15.12 (2000 rounds)

- 005 : Updating 3-fold repetition part (previous version never reached draw games) : Elo difference: 24.36 +/- 21.45 (1000 rounds)

- 004 : Basic time management : Elo difference: 151.35 +/- 53.34 (200 rounds)

- 003 : Code clean

- 002 : Killer heuristic : Elo difference: 79.53 +/- 34.28 (400 rounds)

- 001 : SEE on capture : Elo difference: 148.87 +/- 76.03 (60 rounds)

- 000 : Blank engine : Plain alpha/beta, basic eval : material + PQST + mobility
