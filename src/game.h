#ifndef _GAME_H
#define _GAME_H

#include <chrono>
#include <vector>
#include <string>
#include <map>
#include "move.h"
#include "constants.h"
#include "position.h"
#include "search.h"


class Game {

    friend class Uci;
    friend class Web;
    friend class Tests;

    public:
        Game();

        bool AppendMove(const char*);
        void Debug(const char* s);
        Position GetBoard();
        std::string GetFEN();
        bool InitGame(const char * schema);
        bool LoadFEN(const char*);
        void PlayTurn();
        void PrintCastlingRookMove(Move &);
        void SwitchCurrentPlayer();
        void SetDisplayMoves(bool b);
        void Perft(int maxDepth);
        void SetBlackTime(int);
        void SetMaxDepth(int);
        void SetMovesToGo(int);
        bool SetPlayerMove(const char*);
        void SetRandomness(int i);
        void SetUciProtocol(bool);
        void SetWhiteTime(int);
        int GetHistorySize();
        bool UciMove(const char* move);
        int InsertHistoryMoves(std::string input);

    private:
        Position m_board;
        bool m_checkMoveOnly = false;
        bool m_displayMoves = false;
        bool m_uciProtocol = false;
        std::vector<Move> m_history;
        std::string m_inputSrcSquare;
        std::string m_inputDstSquare;
        int m_maxDepth = 4;
        int m_ply = 0;
        std::vector<Move> m_pvLine;
        int m_randomness = 0;
        int m_turn = 0;
        int m_movesToGo = 0;
        int m_whiteTime = 0;
        int m_blackTime = 0;
};

#endif
