#include "tests.h"
#include "game.h"
#include "hashtables.h"
#include "settings.h"

#include <iostream>

int Tests::SplitPerft(Position& b, int maxDepth, bool displayMoves) {
    int nodes = 0;
    int rootDepth = 0;

    std::vector<Move> nextMoves;
    b.GetPseudoLegalMoves(b.m_plyPlayer, nextMoves);

    BoardFlags bFlags = BoardFlags(b);
    //Position refBoard(b);

    for (auto &m: nextMoves) {

        int _splitNodes = 0;

        b.Make(m);
        
        if (! b.IsPlayerUnderCheck(m.m_srcSide)) {
            Perft(b, rootDepth, maxDepth, _splitNodes);

            if (_splitNodes != 0) {
                nodes += _splitNodes;

                if (displayMoves) {
                    std::cout << SquareTool::ToString(m.GetSrcSquare());
                    std::cout << SquareTool::ToString(m.GetDstSquare());
                    if (m.GetPawnPromotionId()) {
                        std::cout << Piece::GetPieceName(m.GetPawnPromotionId(), m.m_srcSide);
                    }
                    std::cout << ": " << _splitNodes << "\n";
                }
            }
        }
        b.Unmake(m, bFlags);
        //if (!Position::Identical(refBoard,b)) {
        //    std::cout << "Issue reverting " << m.ToCoords() << "\n";
        //    assert(false);
        //}
    }

    return nodes;
}


void Tests::Perft(Position& b, int depth, int maxDepth, int& nodes) {

    if (depth == maxDepth) {
        nodes++;
    }
    else {
        
        std::vector<Move> nextMoves;
        b.GetPseudoLegalMoves(b.m_plyPlayer, nextMoves);

        BoardFlags bFlags = BoardFlags(b);
        //Position refBoard(b);

        for(auto& m : nextMoves) {

            b.Make(m);
            
            if (! b.IsPlayerUnderCheck(m.m_srcSide)) {
                Perft(b, depth+1, maxDepth, nodes);
            }
            b.Unmake(m, bFlags);
            //if (!Position::Identical(refBoard,b)) {
            //    std::cout << "Issue reverting " << m.ToCoords() << "\n";
            //    assert(false);
            //}
        }
    }
}

void Tests::Test3FoldScore() {
    std::cout <<"Tests::Test3FoldScore\n";
    Game game;
    game.LoadFEN("rk6/8/8/8/8/8/7r/1K6 b - - 0 1");
    U64 key = Zobrist::GetHashCode(game.m_board);
    Search::THREE_FOLD_MAP[key] = 2;

    game.LoadFEN("rk6/8/8/8/8/8/7r/K7 w - - 0 1");
    std::cout << game.m_board.ToString();

    int nodes = 0;
    PV_LINE iterationPV;
    std::vector<Move> previousIterationPV;
    Search::REF_TIME = Search::Now();
    Settings::GIVEN_TIME_SECS = 10;
    float score = Search::AlphaBeta(game.m_board, 1, -INF, INF, nodes, iterationPV, previousIterationPV, 0, true);
    std::cout << "score : " << score << "\n";

    assert(score == 0);
}

/***
 * Things to test : captures, enPassant, castling rights
 ***/
void Tests::TestZobristHashing() {
    std::cout <<"Tests::TestZobristHashing\n";
    std::cout << std::hex;

    Game game1;
    game1.LoadFEN("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    game1.AppendMove("e2e4");
    game1.AppendMove("g8f6");
    game1.AppendMove("b1c3");
    game1.AppendMove("f6e4");
    U64 key1 = Zobrist::GetHashCode(game1.m_board);
    std::cout << game1.m_board.ToString();
    std::cout << "key : " << key1 << "\n";
    Position p1(game1.m_board);

    std::cout <<"\n";

    Game game2;
    game2.LoadFEN("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    game2.AppendMove("b1c3");
    game2.AppendMove("g8f6");
    game2.AppendMove("e2e4");
    game2.AppendMove("f6e4");
    std::cout << game2.m_board.ToString();
    U64 key1bis = Zobrist::GetHashCode(game2.m_board);
    std::cout << "key : " << key1bis << "\n";
    Position p2(game2.m_board);

    assert(Position::Identical(p1,p2));
    assert(key1 == key1bis);

}
