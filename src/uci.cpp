#include "search.h"
#include "settings.h"
#include "uci.h"

#include <iostream>
#include <regex>
#include <string>
#include <thread>

int Uci::Listen(Game &game) {
    std::string input;

    while(true) {
        std::getline(std::cin, input);

        if (input == "" || input[0] == '#') {
            continue;
        }

        if (input == "uci") {
            game.SetUciProtocol(true);
            std::cout << "id name Chessika" << std::endl
                      << "id author Laurent 'MrPingouin' Chea" << std::endl
                      << "option name MaxDuration type spin default 1 min 1 max 60" << std::endl
                      << "option name MaxDepth type spin default 10 min 1 max 10" << std::endl
                      << "option name ShowMoveOrdering type check default false" << std::endl
                      << "option name ShowCalculation type check default true" << std::endl
                      << "option name WebMode type check default false" << std::endl
                      << "uciok" << std::endl;
        }

        // Options handling

        else if (input.rfind("setoption", 0) == 0) {
            const std::regex optionRxp("setoption name ([a-zA-Z-_0-9]+) value ([A-Za-z0-9-_]+)");
            std::smatch match;

            if (std::regex_search(input, match, optionRxp) && match.size() > 1) {
                if (match.str(1) == "MaxDuration") {
                    Settings::MAX_DURATION_SECS = atoi(match.str(2).c_str());
                } 
                else if (match.str(1) == "MaxDepth") {
                    Settings::MAX_DEPTH = atoi(match.str(2).c_str());
                } 
                else if (match.str(1) == "ShowMoveOrdering") {
                    Settings::SHOW_MOVE_ORDERING = (match.str(2) == "true");
                } 
                else if (match.str(1) == "ShowCalculation") {
                    Settings::SHOW_MOVE_CALCULATION = (match.str(2) == "true");
                } 
                else if (match.str(1) == "WebMode") {
                    Settings::WEB_MODE = (match.str(2) == "true");
                } 
                else {
                    std::cout << "info INVALID OPTION " << match.str(1) << std::endl;
                }
            }
        }
        else if (input == "stop") {
            Search::Stop();
        }
        else if (input == "isready") {
            std::cout << "readyok" << std::endl;
        }
        else if (input.rfind("perft", 0) == 0) {
            const std::regex valueRxp("perft ([0-9]+)");
            std::smatch match;
            if (std::regex_search(input, match, valueRxp) && match.size() > 1) {
                int depth = atoi(match.str(1).c_str());
                game.Perft(depth);
                return 0;
            }
        }
        else if (input == "ucinewgame") {
            game.m_board.Clear();
            game.m_pvLine.clear();
            game.LoadFEN("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        }

        /**
         * Loading positions
         */
        
        else if (input.rfind("position startpos", 0) == 0) {
            if (game.InsertHistoryMoves(input) == -1)
                return -1;
        }
        else if (input.rfind("position fen", 0) == 0) {
            const std::regex fenRxp("position fen (.*)");
            std::smatch match;

            if (std::regex_search(input, match, fenRxp) && match.size() > 1) {
                game.LoadFEN(match.str(1).c_str());
            }
            else {
                return 0;
            }
        }

        else if (input.rfind("go", 0) == 0) {
            const std::regex goRxp("^go[ ]*(wtime ([0-9]+))?[ ]*(btime ([0-9]+))?[ ]*(movestogo ([0-9]+))?[ ]*(depth ([0-9]+))?");
            std::smatch match;
            if (std::regex_search(input, match, goRxp) && match.size() > 1) {
                if (match.str(2) != "") {
                    game.SetWhiteTime(atoi(match.str(2).c_str()));
                }
                if (match.str(4) != "") {
                    game.SetBlackTime(atoi(match.str(4).c_str()));
                }
                if (match.str(6) != "") {
                    game.SetMovesToGo(atoi(match.str(6).c_str()));
                }
                if (match.str(8) != "") {
                    game.SetMaxDepth(atoi(match.str(8).c_str()));
                }

                if (Settings::WEB_MODE) {
                    game.PlayTurn();
                }
                else {
                    std::thread t(&Game::PlayTurn, std::ref(game));
                    t.detach();
                }
            }
            else {
                std::cout << "info INVALID GO" << std::endl;
                return -1;
            }
        }
        else if (input == "d") {
            std::cout << game.m_board.ToString();
        }
        else if (input == "quit") {
            return 0;
        }
        else  {
            std::cout << "Unknown command: " << input << std::endl;
        }
    }
    return 0;
}

