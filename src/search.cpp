#include "eval.h"
#include "constants.h"
#include "hashtables.h"
#include "moveGen.h"
#include "position.h"
#include "search.h"
#include "settings.h"

#include <algorithm>
#include <iostream>
#include <utility>

#define DEBUG 1


bool Search::TIME_OUT = false;
timePoint_t Search::REF_TIME;
std::map<U64, int> Search::THREE_FOLD_MAP;

// Lol, depth 15, you WISH.
std::vector<Move> KILLER_MOVES[15];
const int MAX_KILLER_MOVES_BY_DEPTH = 3;

/**
 * Returns a high_resolution_clock time point
 */
timePoint_t Search::Now() {
    return std::chrono::high_resolution_clock::now();
}


float Search::ElapsedTimeSecs(timePoint_t refTime) {
    return (float)((Now() - refTime).count()) / std::chrono::high_resolution_clock::period::den;
}

/**
 * Checks if GIVEN_TIME_SECS has been passed
 */
bool Search::IsTimeOut() {
    if (TIME_OUT)
        return true;

    if (ElapsedTimeSecs(REF_TIME) > Settings::GIVEN_TIME_SECS) {
        TIME_OUT = true;
    }
    return TIME_OUT;
}

/**
 * Triangular PV table result is reversed.
 * This function reverses the PV_LINE object into a vector<Move>
 */
void Search::TranslatePV(PV_LINE &pvLine, std::vector<Move> &resultPV) {
    resultPV.clear();
    for(unsigned int i=0; i<pvLine.m_moveCount; i++) {
        resultPV.push_back(pvLine.m_line[i]);
    }
}


/**
 * Iterative Deepening entry point
 */
void Search::IterativeSearch(Position& b, int& totalNodes, std::vector<Move> &resultPV) {

    REF_TIME = Now();

    int depth = 1;

    while (true) {
        if (depth > Settings::MAX_DEPTH)
            break;

        //std::cout << "info start depth " << depth << "-------------- " << std::endl;
        timePoint_t depthRefTimePoint = Now();
        int nodes = 0;

        std::vector<Move> previousIterationPV(resultPV);
        Position tmpBoard(b);

        PV_LINE iterationPV;
        float posValue = AlphaBeta(tmpBoard, depth, -INF, INF, nodes, iterationPV, previousIterationPV, 0, true);

        totalNodes += nodes;

        if (TIME_OUT || iterationPV.m_moveCount == 0) {
            std::cout << "info search timeout at depth " << depth << std::endl;
            break;
        }


        TranslatePV(iterationPV, resultPV);
        resultPV[0].m_value = posValue;

        float depthElapsedTimeSecs = ElapsedTimeSecs(depthRefTimePoint);

        // Logging pv infos
        if (Settings::SHOW_MOVE_CALCULATION) {
            std::cout << "info depth " << depth
                      << " nps " << (int)(nodes/depthElapsedTimeSecs)
                      << " score cp " << (int)posValue 
                      << " nodes " << nodes 
                      << " pv";
            for(const auto &move : resultPV) {
                std::cout << " " << move.ToCoords();
                //std::cout << " " << move.ToString();
            }
            std::cout << std::endl;
        }

        if (std::abs(posValue) >= VALUE_MATE) {
            resultPV[0].m_checkmate = true;
            break;
        }
        
        
        // Do we have enough time to calculate next iteration ?
        // This factor is completly empirical.
        
        const int DEPTH_EVAL_TIME_MULTIPLIER = 4;
        float totalTimeSecs = ElapsedTimeSecs(REF_TIME);
        if ((depthElapsedTimeSecs * DEPTH_EVAL_TIME_MULTIPLIER) > (Settings::GIVEN_TIME_SECS - totalTimeSecs)) {
            std::cout << "info INTERRUPT at " << totalTimeSecs << ", remaining time " << (Settings::GIVEN_TIME_SECS - totalTimeSecs) << " seconds" << std::endl;
            break;
        }
        depth++;
    }

    // We keep this to tell the engine that even depth 1 hasn't be calculated in time
    if (TIME_OUT && depth > 1)
        TIME_OUT = false;
}

float Search::AlphaBeta(Position& p, unsigned int depth, 
                        int alpha, int beta, int& nodes, 
                        PV_LINE& pvLine, std::vector<Move>& previousPV, unsigned int currentDepth, 
                        bool nullMoveAllowed) {

    //std::cout << "[" << currentDepth << "] AlphaBeta alpha:" << alpha << ", beta:" << beta << std::endl;
    //std::cout << std::endl << p.ToString();


    if (IsTimeOut()) {
        return -INF;
    }

    // Check extension
    //if (depth == 0 && p.IsPlayerUnderCheck(p.m_plyPlayer)) {
    //    depth++;
    //}
    
    pvLine.m_moveCount = 0;

    if (depth == 0) {
        int playerSide = p.m_plyPlayer == 1 ? 1 : - 1;
        nodes++;
        Eval boardEval(p);
        return playerSide*boardEval.GetValue();
    }

    BoardFlags bFlags = BoardFlags(p);

    // Null-move pruning
    if (nullMoveAllowed && !p.IsPlayerUnderCheck(p.m_plyPlayer) && depth > 3) {
        const int R = 3;
        PV_LINE nullDepthLine;

        p.MakeNullMove();
        float nullScore = -AlphaBeta(p, depth-1 - R, -beta, -beta+1, nodes, nullDepthLine, previousPV, currentDepth+2, false);
        p.UnmakeNullMove(bFlags);

        if (nullScore >= beta) {
            return nullScore;
        }
    }

    // Generate pseudo legal moves and sort them
    std::vector<Move> nextMoves;
    p.GetPseudoLegalMoves(p.m_plyPlayer, nextMoves);
    SortMoves(p, nextMoves, previousPV, currentDepth);


    PV_LINE depthLine;
    float best = -INF;


    for (auto &move : nextMoves) {
        if (IsTimeOut()) {
            pvLine.m_moveCount = 0;
            return 0;
        }

#if DEBUG
        Position refTestPosition(p);
#endif

        p.Make(move);

        if (MoveGen::IsIllegal(p, move)) {
            p.Unmake(move, bFlags);
            assert(Position::Identical(p, refTestPosition));
            continue;
        }
        //std::cout << "[" << currentDepth << "] " << move.ToCoords() << std::endl;
        //std::cout << p.ToString();

        // 3 fold repetition
        bool draw = false;
        U64 key = Zobrist::GetHashCode(p);
        if (THREE_FOLD_MAP.find(key) != THREE_FOLD_MAP.end()) {
            THREE_FOLD_MAP[key]++;
            if (THREE_FOLD_MAP[key] >= 3) {
                draw = true;
            }
        }
        else {
            THREE_FOLD_MAP[key] = 1;
        }
        
        float v = 0;
        if (!draw)
            v = -AlphaBeta(p, depth-1, -beta, -alpha, nodes, depthLine, previousPV, currentDepth+1, nullMoveAllowed);

        p.Unmake(move, bFlags);
        THREE_FOLD_MAP[key]--;

        assert(Position::Identical(p, refTestPosition));

        // Beta-cutoff
        if (v >= beta) {

            // Storing move for Killer Heuristic
            if (!move.m_capture) {
                auto killerMoveIt = std::find (KILLER_MOVES[currentDepth].begin(), KILLER_MOVES[currentDepth].end(), move);
                if (killerMoveIt == KILLER_MOVES[currentDepth].end()) {
                    KILLER_MOVES[currentDepth].push_back(move);
                }
            }
            return beta;
        }

        if (v > best) {
            best = v;


            if (v > alpha) {
                alpha = v;
                pvLine.m_line[0] = move;
                if (draw) {
                    depthLine.m_moveCount = 0;
                }

                for(unsigned int j=0; j<depthLine.m_moveCount; j++) {
                   pvLine.m_line[1+j] = depthLine.m_line[j];
                }
                pvLine.m_moveCount = depthLine.m_moveCount + 1;
            }
        }
    }

    if (best == -INF) {
        pvLine.m_moveCount = 0;
        if (p.IsPlayerUnderCheck(p.m_plyPlayer)) {
            return -(VALUE_MATE+depth);
        }
        else {
            // Stalemate
            return 0;
        }
    }

    return best;
}


/**
 * Static exchange evaluation (SEE : move ordering on non-capturing moves)
 * we only focus on the destination square
 * and simulate some plies by attacking the same square with the least valuable attacker)
 * if there is no attacker, score is 0
 * if there's one, we return the value of the current piece minus the SEE score one step deeper
 */

int Search::CaptureAndSee(Position &p, Move &m) {
    BoardFlags bFlags = BoardFlags(p);
#if DEBUG
    Position refTestPosition(p);
#endif

    p.Make(m);

    int score = 0;

    if (!MoveGen::IsIllegal(p, m)) {
        score = PIECE_VALUES[m.m_dstId] - See(p, m.m_dstSquare);
    }

    p.Unmake(m, bFlags);

    assert(Position::Identical(p, refTestPosition));

    return score;
}


int Search::See(Position &p, const Square& dstSq) {
    int srcSq = p.GetSmallestAttacker(dstSq);

    if (srcSq >= 0) {
        PieceId dstId = p.SquareId(dstSq);

        if (dstId == KING_ID)
            return 0;
        PieceId srcId = p.SquareId(srcSq);
        Color opp = Position::GetOpponent(p.m_plyPlayer);

        Move move(p.m_plyPlayer, opp, srcId, dstId, srcSq, dstSq);

        BoardFlags bFlags = BoardFlags(p);

#if DEBUG
        Position refTestPosition(p);
#endif

        int score = 0;
        p.Make(move);

        if (!MoveGen::IsIllegal(p, move)) {
            score = PIECE_VALUES[dstId] - See(p, dstSq);
        }

        p.Unmake(move, bFlags);

        assert(Position::Identical(p, refTestPosition));

        return score;

    }

    return 0;
}

int Search::MVV_LVA(Move &m) {
    const int CAPTURE_BONUS = 50;

    // Empty ID has no value : no capture makes the sortScore to 0
    // Most valuable victims are simply the different pieces ids in ascending sort.
    const int MVV_SCORES[7] = { 0,  PIECE_VALUES[PAWN_ID] ,
                                    PIECE_VALUES[KNIGHT_ID],
                                    PIECE_VALUES[BISHOP_ID],
                                    PIECE_VALUES[ROOK_ID],
                                    PIECE_VALUES[QUEEN_ID], 0 };

    // Least valuable attacker is almost the invert.
    // For now, I decided that capturing with a pawn was more valuable than capturing with a king,
    // as it could change its defense.
    // In LVA table, EmptyID (index 0) should be obviously never requested.
    const int LVA_SCORES[7] = { 0,  PIECE_VALUES[QUEEN_ID], 
                                    PIECE_VALUES[ROOK_ID],
                                    PIECE_VALUES[BISHOP_ID],
                                    PIECE_VALUES[KNIGHT_ID],
                                    PIECE_VALUES[KING_ID],
                                    PIECE_VALUES[PAWN_ID] };
    
    return CAPTURE_BONUS + MVV_SCORES[m.m_dstId] * (LVA_SCORES[m.m_srcId]) / 10000;
}

void Search::Stop() {
    std::cout << "info STOP invoked" << std::endl;
    TIME_OUT = true;
}

/***
 * By default, all scores are set to 0
 *
 * For now, the only sort we have are : 
 * Previous pv first
 * SEE on captures 
 * Killer heuristic
 *
 */
void Search::SortMoves(Position &b, std::vector<Move>& nextMoves, std::vector<Move>& previousPV, unsigned int currentDepth) {
    
    for (auto &m: nextMoves) {
        if (m.m_capture) {
            //m.m_sortScore = MVV_LVA(m);
            m.m_sortScore = CaptureAndSee(b, m);
        }
    }

    // Killer Heuristic 
    if (currentDepth > 2) { 
        for(auto &m : KILLER_MOVES[currentDepth-2]) {
            auto it = std::find(nextMoves.begin(), nextMoves.end(), m);
            if (it != nextMoves.end()) {
                it->m_sortScore = 10;
            }
        }
    }

    // Previous pv first

    if (previousPV.size() > 0 && currentDepth < previousPV.size()) {
        Move &m = previousPV[currentDepth];
        auto it = std::find(nextMoves.begin(), nextMoves.end(), m);
        if (it != nextMoves.end()) {
            it->m_sortScore = 10000;
        }
    }

    std::sort(nextMoves.begin(), nextMoves.end());

    // Displaying move ordering result

    if (Settings::SHOW_MOVE_ORDERING && currentDepth == 0) {
        for (auto m : nextMoves) {
            std::cout << "  " << m.ToString() << ":" << m.m_sortScore;
        }
        std::cout << std::endl;
    }
}
