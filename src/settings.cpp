#include "settings.h"
#include "constants.h"

int Settings::MAX_DEPTH = 10;
float Settings::GIVEN_TIME_SECS = 0;
long int Settings::MAX_DURATION_SECS = 60;
bool Settings::SHOW_MOVE_ORDERING = false;
bool Settings::SHOW_MOVE_CALCULATION = true;
bool Settings::WEB_MODE = false;
