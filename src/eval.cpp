#include <map>
#include <algorithm>
#include <stdexcept>
#include <string>
#include <iomanip>
#include <tuple>
#include <vector>
#include "eval.h"
#include "moveGen.h"
#include "math.h"

Eval::Eval(Position &p) {
    bool isEndGame = false;

    m_p1boardMaterial = GetBoardMaterialValue(p, COLOR_WHITE);
    m_p2boardMaterial = GetBoardMaterialValue(p, COLOR_BLACK);

    m_p1positionalValue = GetPositionalValue(p, COLOR_WHITE, m_p1boardMaterial);
    m_p2positionalValue = GetPositionalValue(p, COLOR_BLACK, m_p2boardMaterial);

    //m_p1bishopPairBonus = HasPair(p, BISHOP_ID, COLOR_WHITE);
    //m_p2bishopPairBonus = HasPair(p, BISHOP_ID, COLOR_BLACK);

    //m_p1knightPairBonus = HasPair(p, KNIGHT_ID, COLOR_WHITE);
    //m_p2knightPairBonus = HasPair(p, KNIGHT_ID, COLOR_BLACK);

    /* m_p1doublePawnFiles = GetDoublePawnFileCount(COLOR_WHITE, pt); */
    /* m_p2doublePawnFiles = GetDoublePawnFileCount(COLOR_BLACK, pt); */

    //if (m_p2doublePawnFiles > 0) {
    //    std::cout << "--------------" << std::endl;
    //    std::cout << move.ToString() << std::endl;
    //    std::cout << b.ToString();
    //    std::cout << "--------------" << std::endl;
    //}

    std::vector<Move> p1legalMoves;
    std::vector<Move> p2legalMoves;

    p.GetPseudoLegalMoves(COLOR_WHITE, p1legalMoves);
    p.GetPseudoLegalMoves(COLOR_BLACK, p2legalMoves);

    m_p1capturableValue = GetCapturableValue(m_p1bestCapture, p1legalMoves);
    m_p2capturableValue = GetCapturableValue(m_p2bestCapture, p2legalMoves);

    m_p1mobility = p1legalMoves.size();
    m_p2mobility = p2legalMoves.size();
}


float Eval::GetValue() {
    float res = 0;

    const float posFactor = 2;
    const float materialFactor = 3;
    const float mobilityFactor = 1;

    res += (m_p1positionalValue - m_p2positionalValue) * posFactor;
    res += (m_p1boardMaterial - m_p2boardMaterial) * materialFactor;
    res += (m_p1capturableValue - m_p2capturableValue);
    res += (m_p1mobility - m_p2mobility) * mobilityFactor;

//    res += (m_p1bishopPairBonus - m_p2bishopPairBonus);
//    res += (m_p1knightPairBonus - m_p2knightPairBonus);

    // TODO : rook open file bonus 
    // TODO : fork bonus 
    // TODO : rook x-ray bonus 

    /*
    const float doublePawnFileFactor = 10;
    const int initialPiecePenaltyTurn = 6;
    const float initialPieceCountPenalty = 30;

    if (playerSide == COLOR_WHITE) {
        if (m_p1doublePawnFiles > 0) {
            res -= m_p1doublePawnFiles * doublePawnFileFactor;
        }
        if (turn > 1 && turn < initialPiecePenaltyTurn && m_p1pieceInitialStateCount >= 2) {
            res -= m_p1pieceInitialStateCount * initialPieceCountPenalty;
        }
    }
    else {
        if (m_p2doublePawnFiles > 0) {
            res += m_p2doublePawnFiles * doublePawnFileFactor;
        }
        if (turn > 1 && turn < initialPiecePenaltyTurn && m_p2pieceInitialStateCount >= 2) {
            res += m_p2pieceInitialStateCount * initialPieceCountPenalty;
        }
    }
    */

    return res;
}


bool Eval::IsPlayerCheck(Color c) {
    if (c == COLOR_WHITE)
        return m_p1check == true;
    return m_p2check == true;
}

/**
 * Returns the sum of each piece positional value
 */
float Eval::GetPositionalValue(Position &p, Color c, float materialValue) {
    float res = 0;

    U64 srcBits = 0;
   
    srcBits =  p.m_pieces[PAWN_ID] & p.m_colors[c];
    while(srcBits) {
        int sq = Position::BitScanForward(Position::LS1B(srcBits));
        res += PAWN_SQUARE_VALUES[c][sq];
        Position::ResetLS1B(srcBits);
    }

    srcBits =  p.m_pieces[KNIGHT_ID] & p.m_colors[c];
    while(srcBits) {
        int sq = Position::BitScanForward(Position::LS1B(srcBits));
        res += KNIGHT_SQUARE_VALUES[c][sq];
        Position::ResetLS1B(srcBits);
    }

    srcBits =  p.m_pieces[BISHOP_ID] & p.m_colors[c];
    while(srcBits) {
        int sq = Position::BitScanForward(Position::LS1B(srcBits));
        res += BISHOP_SQUARE_VALUES[c][sq];
        Position::ResetLS1B(srcBits);
    }

    srcBits =  p.m_pieces[ROOK_ID] & p.m_colors[c];
    while(srcBits) {
        int sq = Position::BitScanForward(Position::LS1B(srcBits));
        res += ROOK_SQUARE_VALUES[c][sq];
        Position::ResetLS1B(srcBits);
    }

    srcBits =  p.m_pieces[QUEEN_ID] & p.m_colors[c];
    while(srcBits) {
        int sq = Position::BitScanForward(Position::LS1B(srcBits));
        res += QUEEN_SQUARE_VALUES[c][sq];
        Position::ResetLS1B(srcBits);
    }

    srcBits =  p.m_pieces[KING_ID] & p.m_colors[c];
    int sq = Position::BitScanForward(Position::LS1B(srcBits));

    if (materialValue < END_GAME_MATERIAL_VALUE)
        res += KING_ENDGAME_SQUARE_VALUES[c][sq];
    else
        res += KING_MIDGAME_SQUARE_VALUES[c][sq];

    return res;
}


/**
 * Returns the UNSIGNED sum of each capturable piece
 * We naturally ignore the king value
 * bestCapture is an out parameter
 */
int Eval::GetCapturableValue(int& bestCapture, const std::vector<Move>& moves) {

    int res = 0;
    bestCapture = 0;

    for (const auto& m : moves) {
        if (m.m_capture) {
            if (m.m_dstId == KING_ID)
                continue;

            int targetValue = PIECE_VALUES[m.m_dstId];
            res += targetValue;

            if (targetValue > bestCapture)
                bestCapture = targetValue;
        }
    }

    return res;
}


int Eval::GetDoublePawnFileCount(Color /*c*/) {
    int res = 0;
    int fileCount[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // ignoring indexes out-of-board indexes
    /* for (auto &cell: pt.m_pieceList[clr]) { */
    /*     if (cell.m_pieceId == PAWN_ID) */
    /*         fileCount[FILE_LUT[cell.m_square]]++; */
    /* } */

    /* for (int i=0; i<10; i++) { */
    /*     if (fileCount[i] > 1) */
    /*         res++; */
    /* } */

    return res;
}

/**
 * Returns the sum of all present pieces values 
 */
float Eval::GetBoardMaterialValue(Position &p, Color c) {
    float res = 0;
    for(unsigned int id=PAWN_ID; id<KING_ID; id++) {
        U64 srcBits = p.m_pieces[id] & p.m_colors[c];
        //std::cout << Position::Debug(srcBits);

        while(srcBits) {
            res += PIECE_VALUES[id];
            Position::ResetLS1B(srcBits);
        }
    }
    return res;
}


int Eval::HasPair(Position &p, PieceId id, Color c) {
    U64 srcBits = p.m_pieces[id] & p.m_colors[c];
    short pieceCount = 0;

    while(srcBits) {
        pieceCount++;
        Position::ResetLS1B(srcBits);
    }

    if (pieceCount > 1) {
        return 30;
    }
    return 0;
}

// Tropism : number of piece targetting the king, blocked or not
float Eval::GetTropism(Position& /*b*/, int /*playerSide*/, std::vector<Move>& /*oppMoves*/) {
    float res = 0;
    return res;
}
