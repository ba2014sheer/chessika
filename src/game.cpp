#include "eval.h"
#include "game.h"
#include "moveGen.h"
#include "search.h"
#include "settings.h"
#include "tests.h"
#include "hashtables.h"

#include "stdlib.h"
#include <algorithm>
#include <string>
#include <iostream>
#include <cstring>
#include <mutex>
#include <regex>
#include <stdexcept>
#include <unistd.h>
#include <vector>


Game::Game() {}

bool Game::LoadFEN(const char* fen) {

    // Regex groups
    // 1:board  2:player  3:castling  5:en passant 7:ply 8:turn       
    const std::regex re("^([RNBQKPrnbqkp012345678/]+) ([wb]) (([KQkq]*)|-) (([a-h][36])|-) ([01]) ([0-9]+)$");
    std::smatch match;
    std::string str(fen);

    std::string boardSchema;

    if (std::regex_search(str, match, re) && match.size() > 1) {

        m_board.m_plyPlayer = COLOR_WHITE;
        if (match.str(2) == "b")
            m_board.m_plyPlayer = COLOR_BLACK;

        // Castling

        short castleFlags = 0;

        if (match.str(3).find("K") != std::string::npos)
            castleFlags |= 1<<3; 
        
        if (match.str(3).find("Q") != std::string::npos)
            castleFlags |= 1<<2; 

        if (match.str(3).find("k") != std::string::npos)
            castleFlags |= 1<<1; 
        
        if (match.str(3).find("q") != std::string::npos)
            castleFlags |= 1; 

        m_board.m_castleFlags = castleFlags;

        if (match.str(5).find("-") == std::string::npos) {
            m_board.m_enPassantSquare = SquareTool::FromCoords(match.str(5).c_str());
        }
        else {
            m_board.m_enPassantSquare = -1;
        }

        m_ply = atoi(match.str(7).c_str());
        m_turn = atoi(match.str(8).c_str());

        // Initializing board 
        boardSchema = match.str(1);
        m_board.InitPlayerPieces(boardSchema.c_str());

        return true;
    }
    return false;
}


void Game::Debug(const char* s) {
    if (m_uciProtocol) {
        std::cout << "info " << s << std::endl;
    }
    else {
        std::cerr << s << std::endl;
    }
}


void Game::Perft(int maxDepth) {
    if (maxDepth > 0)
        maxDepth--;
    else
        maxDepth = 0;

    int nodes = Tests::SplitPerft(m_board, maxDepth, m_displayMoves);
    std::cout << GetFEN() << " ;D" << maxDepth+1 << " " << nodes << std::endl;
}

void Game::PrintCastlingRookMove(Move &m) {
    if (m.m_castle) {
        if(m.m_castle & 1<<3)
            std::cout << "info castling_rook h1f1" << std::endl;
        else if(m.m_castle & 1<<2)
            std::cout << "info castling_rook a1d1" << std::endl;
        else if(m.m_castle & 1<<1)
            std::cout << "info castling_rook h8f8" << std::endl;
        else
            std::cout << "info castling_rook a8d8" << std::endl;
    }
}


void Game::PlayTurn() {
    TurnStatus moveStatus = LEGAL_MOVE;
    std::vector<Move> resultPV;

    // This is legit for web mode only
    if (m_pvLine.size() > 1) {
        // We want to start from the previous best search
        // Removing the previous iteration plies (2 plies)
        m_pvLine.erase(m_pvLine.begin(), m_pvLine.begin()+2);
    }

    Move pickedMove;
    int nodes = 0;

    // Caution : UCI times are in milliseconds
    int playerTimeMs = m_board.m_plyPlayer == COLOR_WHITE ? m_whiteTime : m_blackTime;
    int remainingTimeMicroSecs = playerTimeMs * 1000;

    int givenEvalTimeMicroSecs = 0;

    int currentTurn = (m_board.m_ply/2 + 1);
    int movesToGo = m_movesToGo/2;
    std::cout << "info current turn : " << currentTurn << std::endl;
    std::cout << "info remainingTime : " << (remainingTimeMicroSecs) << std::endl;

    if (movesToGo == 0) {
        movesToGo = (40 - currentTurn)/2;
        if (movesToGo < 0)
            movesToGo = 1;
        std::cout << "info [forced] moves to go/2 : " << movesToGo << std::endl;
    }
    else
        std::cout << "info moves to go/2 : " << movesToGo << std::endl;

    givenEvalTimeMicroSecs = (remainingTimeMicroSecs / ((double)movesToGo));

    Settings::GIVEN_TIME_SECS = givenEvalTimeMicroSecs / (float)MICRO_SECS_TO_SECS;
    std::cout << "info given eval time (s) : " << Settings::GIVEN_TIME_SECS << std::endl;

    Search::IterativeSearch(m_board, nodes, resultPV);

    // Refreshing pvLine
    m_pvLine = resultPV;

    if (m_pvLine.size() > 0) {
        pickedMove = m_pvLine[0];
        if (pickedMove.m_checkmate) {
            if (m_board.m_plyPlayer == COLOR_WHITE)
                moveStatus = CHECKMATE_P1;
            else
                moveStatus = CHECKMATE_P2;
        }
    }
    else {
        Eval bs(m_board);

        if (!bs.IsPlayerCheck(COLOR_WHITE) && !bs.IsPlayerCheck(COLOR_BLACK)) {
            moveStatus = STALEMATE;
        }
        else {
            if (Search::TIME_OUT == true) {
                moveStatus = TIMEOUT;
            }
            else {
                if (bs.IsPlayerCheck(COLOR_WHITE)) {
                    moveStatus = CHECKMATE_P1;
                }
                else if (bs.IsPlayerCheck(COLOR_BLACK)){
                    moveStatus = CHECKMATE_P2;
                }
            }
        }
    }

    if (m_pvLine.size() > 0) {
        std::cout << "bestmove " << pickedMove.ToCoords() << std::endl;
        if (Settings::WEB_MODE) {
            std::cout << "webmove " << pickedMove.ToString() << std::endl;
            PrintCastlingRookMove(pickedMove);
        }
    }
    else {
        // Hack for web mode : if max depth is 0, we're only checking the last move legality
        if (Settings::WEB_MODE && Settings::MAX_DEPTH == 0) {
            std::cout << "webmove " << m_history.back().ToString() << std::endl;
            PrintCastlingRookMove(m_history.back());
        }
        else {
            std::cout << "info no move calculated" << std::endl;
            switch(moveStatus) {
                case STALEMATE:
                    std::cout << "info stalemate" << std::endl;
                    break;
                case CHECKMATE_P1:
                    std::cout << "info checkmate_p1" << std::endl;
                    break;
                case CHECKMATE_P2:
                    std::cout << "info checkmate_p2" << std::endl;
                    break;
                default:
                    std::cout << "info lost_on_time" << std::endl;
            }
            exit(0);
        }
    }
}


std::string Game::GetFEN() {
    std::stringstream ss;

    ss << m_board.ToFEN();
    
    // TODO : handle half move
    ss << "0 " << m_turn;
    return ss.str();
}


void Game::SetMaxDepth(int maxDepth) {
    m_maxDepth = maxDepth;
}


bool Game::SetPlayerMove(const char* move) {
    std::regex re("^([a-h][1-8])([a-h][1-8])$");
    std::smatch match;
    std::string str(move);
    if (std::regex_search(str, match, re) && match.size() > 1) {
        m_inputSrcSquare = match.str(1);
        m_inputDstSquare = match.str(2);
        m_checkMoveOnly = true;
        return true;
    }
    return false;
}


void Game::SetDisplayMoves(bool b) {
    m_displayMoves = b;
}


void Game::SetRandomness(int i) {
    m_randomness = i;
}

        
void Game::SetUciProtocol(bool b) {
    m_uciProtocol = b;
}

bool Game::AppendMove(const char* move) {
    const std::regex re("^([a-h][1-8])([a-h][1-8])[bnrqBNRQ]?$");
    std::smatch match;
    std::string str(move);
    if (std::regex_search(str, match, re) && match.size() > 1) {
        Move inputMove;
        
        if (m_board.DoesMoveExist(inputMove, match.str(1).c_str(), match.str(2).c_str())) {
            m_board.Make(inputMove);
            if (MoveGen::IsIllegal(m_board, inputMove)) {
                return false;
            }
            m_history.push_back(inputMove);
        }
        else {
            return false;
        }

        U64 key = Zobrist::GetHashCode(m_board);

        if (Search::THREE_FOLD_MAP.find(key) != Search::THREE_FOLD_MAP.end()) {
            Search::THREE_FOLD_MAP[key]++;
        }
        else
            Search::THREE_FOLD_MAP[key] = 1;

        return true;
    }
    return false;
}

int Game::GetHistorySize() {
    return m_history.size();
}

int Game::InsertHistoryMoves(std::string input) {
    Search::THREE_FOLD_MAP.clear();
    LoadFEN("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    m_history.clear();
    m_board.m_ply = 0;

    const std::regex moveRxp("[a-h1-8]{4}[brnqBRNQ]?");
    std::smatch match;

    while (std::regex_search(input, match, moveRxp)) { 
        if (!AppendMove(match.str(0).c_str())) {
            std::cout << "info INVALID move " << match.str(0) << std::endl;
            return -1;
        }
        input = match.suffix().str(); 
    } 


    return 0;
}

Position Game::GetBoard() {
    return m_board;
}

void Game::SetWhiteTime(int time) {
    m_whiteTime = time;
}

void Game::SetBlackTime(int time) {
    m_blackTime = time;
}

void Game::SetMovesToGo(int moves) {
    m_movesToGo = moves;
}
