#ifndef _TESTS_H
#define _TESTS_H

#include "position.h"

class Tests {
    public:
        static void Perft(Position&, int depth, int maxDepth, int& nodes);
        static int SplitPerft(Position&, int maxDepth, bool displayMoves);
        static void TestZobristHashing();
        static void Test3FoldScore();
};

#endif
