#ifndef _SETTINGS_H
#define _SETTINGS_H

class Settings {
    public:
        static long int MAX_DURATION_SECS;
        static int MAX_DEPTH;
        static long int GIVEN_TIME_MICROSECS;
        static float GIVEN_TIME_SECS;
        static bool SHOW_MOVE_ORDERING;
        static bool SHOW_MOVE_CALCULATION;
        static bool WEB_MODE;
};

#endif
