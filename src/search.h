#ifndef _SEARCH_H
#define _SEARCH_H

#include "move.h"
#include "position.h"

#include <map>
#include <chrono>
#include <vector>

typedef std::chrono::time_point<std::chrono::high_resolution_clock> timePoint_t;

const int MAX_PV_LENGTH = 20;

typedef struct pv_Line_t {
    unsigned int m_moveCount = 0;
    Move m_line[MAX_PV_LENGTH];
} PV_LINE;


class Search {

    public:
        static bool TIME_OUT;
        static timePoint_t REF_TIME;
        static std::map<U64, int> THREE_FOLD_MAP;

        static float AlphaBeta(Position&, unsigned int depth, int alpha, int beta, 
                               int& nodes, PV_LINE &line, std::vector<Move>& prevPV, 
                               unsigned int currentDepth, bool nullMoveAllowed);
        static int CaptureAndSee(Position &, Move &); 
        static float ElapsedTimeSecs(timePoint_t);
        static void IterativeSearch(Position& b, int& totalNodes, std::vector<Move> &resultPV);
        static int GetCapturable(Position &p, const Square& dstSq);
        static bool IsTimeOut();
        static int MVV_LVA(Move&);
        static timePoint_t Now();
        static int See(Position &p, const Square& sq);
        static void SortMoves(Position&, std::vector<Move> &nextMoves, std::vector<Move> &previousPV, unsigned int currentDepth);
        static void Stop();
        static void TranslatePV(PV_LINE &pvLine, std::vector<Move> &resultPV);
};
    


#endif

