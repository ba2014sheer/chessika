#include "constants.h"
#include "game.h"
#include "moveGen.h"
#include "uci.h"
#include "hashtables.h"
#include "tests.h"

#include <iostream>
#include <unistd.h>


int main() {
    std::cerr << "Chessika v2, by MrPingouin" << std::endl;

    MoveGen::InitFillUpAttacks();
    Zobrist::Init();
    //Tests::TestZobristHashing();
    //Tests::Test3FoldScore();
    //return 0;

    Game game;
    //game.LoadFEN("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    //std::cout << game.GetBoard().ToString();
    //Position p;
    //p = game.GetBoard();
    //std::cout << std::hex << "Key : " << Zobrist::GetHashCode(p) << std::endl;
    //return 0;

    return Uci::Listen(game);
}

