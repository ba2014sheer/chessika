#ifndef BOARD_FLAGS_H
#define BOARD_FLAGS_H

#include <string>
#include <vector>
#include "position.h"
#include "constants.h"
#include "square.h"

class Position;

class BoardFlags {
    public:
        short m_castleFlags = 0;
        Color m_plyPlayer;
        // FIXME : magic number
        Square m_enPassantSquare = -1;

        BoardFlags(const Position&);
};

#endif
