OPENINGS=books/gaviota-starters.pgn
OLD=bin/chessika_no_ordering
NEW=obj/chessika
cutechess-cli -engine cmd=${NEW} name="NEW" -engine cmd=${OLD} name="OLD" -each tc=40/60 proto=uci option.MaxDuration=6 -openings file=${OPENINGS} order=random -repeat -rounds $1 #-debug
#rnbq1rk1/pp4b1/2pp1pp1/3Pp2p/2P1P1nB/2N2N2/PP2BPPP/R2QK2R w KQ - 0 11
